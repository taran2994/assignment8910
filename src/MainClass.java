import com.google.gson.GsonBuilder;

public class MainClass {
	public static void main(String[] args) {

		BlockChain chain1 = new BlockChain(); // creating new block chain

		Block block1 = new Block("This is the first block", "0"); // using 0 as the previous hash for the first block
		chain1.getBlockchain().add(block1);
		chain1.getBlockchain().get(0).mineNonsenseCoin();

		Block block2 = new Block("This is the second block", chain1.getBlockchain().get(chain1.getBlockchain().size()-1).getHash());
		chain1.getBlockchain().add(block2);
		chain1.getBlockchain().get(1).mineNonsenseCoin();

		Block block3 = new Block("This is the third block", chain1.getBlockchain().get(chain1.getBlockchain().size()-1).getHash());
		chain1.getBlockchain().add(block3);
		chain1.getBlockchain().get(2).mineNonsenseCoin();

		Block block4 = new Block("This is the fourth block", chain1.getBlockchain().get(chain1.getBlockchain().size()-1).getHash());
		chain1.getBlockchain().add(block4);
		chain1.getBlockchain().get(3).mineNonsenseCoin();

		if (chain1.transactionValidator()) { // checking if transaction is valid or not
			System.out.println("Transaction is valid and You get Nonsense coin");
		} else {
			System.out.println("Transaction is invalid");
		}
		
		
		String blockchainJson = new GsonBuilder().setPrettyPrinting().create().toJson(chain1);		// to display all the block objects' chain 
		System.out.println(blockchainJson);
		System.out.println("Number of coins distributed "+chain1.getCoinsDistributed());

	}
}
