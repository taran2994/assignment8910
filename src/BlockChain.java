import java.util.ArrayList;

public class BlockChain {
	private int coinsDistributed; // gives the number of coins distributed

	private ArrayList<Block> blockchain = new ArrayList<Block>();

	public ArrayList<Block> getBlockchain() {
		return blockchain;
	}

	public void setBlockchain(ArrayList<Block> blockchain) {
		this.blockchain = blockchain;
	}

	public Boolean transactionValidator() {
		Block currentBlock;
		Block previousBlock;

		for (int i = 1; i < blockchain.size(); i++) {
			currentBlock = blockchain.get(i);
			previousBlock = blockchain.get(i - 1);

			if (!currentBlock.getHash().equals(currentBlock.calculateHash())) {
				return false;
			}

			if (!previousBlock.getHash().equals(currentBlock.getPreviousHash())) {
				return false;
			}
			if(!currentBlock.getHash().substring( 0, 2).equals("00")) {
				System.out.println("This block hasn't been mined");
				return false;
			}
		}
		System.out.println(blockchain.size());
		return true;
	}

	public int getCoinsDistributed() {
		coinsDistributed=blockchain.size();
		return coinsDistributed;
	}

	public void setCoinsDistributed(int coinsDistributed) {
		this.coinsDistributed = coinsDistributed;
	}
	
}
