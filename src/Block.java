public class Block {

	private String hash; // data + previous hash
	private String previousHash;
	private int changingVariable = 0; // changing this variable will give different hash

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getPreviousHash() {
		return previousHash;
	}

	public void setPreviousHash(String previousHash) {
		this.previousHash = previousHash;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	private String data;

	public Block(String data, String previousHash) {
		this.data = data;
		this.previousHash = previousHash;
		this.hash = calculateHash();

	}

	public String calculateHash() {
		String newHash = Crypto.applySha256(previousHash + data + Integer.toString(changingVariable));
		return newHash;
	}

	public void mineNonsenseCoin() {

		String pattern = "00"; // pattern to be matched
		while (!hash.substring(0, 2).equals(pattern)) { // checking if the hash has the "00" pattern
			changingVariable++;
			hash = calculateHash();
		}
		System.out.println("Nonsense coin is mined : " + hash);
	}
	
	
	

}